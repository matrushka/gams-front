# GAMS Front-end development

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# GAMS Airtable Integration setup

The Airtable integration is a PHP script that does three things:

* Gets the newest member data from Airtable
* Gets geolocation data from the Google Geolocating API
* Formats the member data in a way that is expected by the VueJS application

In order to do that, some variables have to be configured in an .env file located in the same directory where the PHP script is located. Please see the .env.example file for reference.

Please note that:

* The path set in OUTPUT_FILE needs to be accessible through the web (the browser will load it directly to populate the map).
* The path set in CACHE_FILE does not need to be public (but it's not a problem if it is).

The Geolocation data is cached in a JSON file to avoid an excessive number of calls to the API. It's safe to remove that file to force a refresh of the geolocation data if you think it's necessary.
