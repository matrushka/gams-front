<?php

require "vendor/autoload.php";

use TANIOS\Airtable\Airtable;
use Dotenv\Dotenv;
use GuzzleHttp\Client;

// Load Dotenv data
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

/***********************************************
*SET UP THE ENVIRONMENT VARIABLES **************
***********************************************/

define("AIRTABLE_API_KEY", $_ENV['AIRTABLE_API_KEY']);
define("AIRTABLE_DATABASE", $_ENV['AIRTABLE_DATABASE']);
define("GOOGLE_GEOCODING_API_KEY", $_ENV['GOOGLE_GEOCODING_API_KEY']);
define("OUTPUT_FILE", $_ENV['OUTPUT_FILE']);
define("CACHE_FILE", $_ENV['CACHE_FILE']);
define("LOG_FILE", $_ENV['LOG_FILE']);

/***********************************************
*LOAD THE CACHE FILE****************************
***********************************************/
$cache_file_contents = @file_get_contents(CACHE_FILE);
$cache = json_decode($cache_file_contents, TRUE) ?? [];

/***********************************************
*LOAD AND PROCESS TAXONOMIES********************
***********************************************/
$professional_sectors = getTaxonomy('Tags-Professional Sectors');
$countries = getTaxonomy('Tags-Countries');
$genders = getTaxonomy('Tags-Genders');
$professions = getTaxonomy('Tags-Professions');
$displays = getTaxonomy('Tags-Display', [
  "Key" => 'key',
]);

/***********************************************
*LOAD AND FORMAT THE DIRECTORY******************
***********************************************/
$directory = getTable('Directory');

// Get pertinent person data and
// group people by matching location (to avoid duplicate calls to the geocoding API)
$people = array_reduce($directory, function($people, $row) use ($displays) {
  $current_location = $row->fields->{'Full Location'};
  
  // Create the location group if it doesn't exist already
  if(!array_key_exists($current_location, $people)) {
    $grouped[$current_location] = [];
  }

  // Add the person to the location group
  // Only if they want to appear in the map
  $display_id = $row->fields->{'Tags-Display'}[0];
  $display_key = $displays['keyed_by_id'][$display_id]['key'];

  // Checking if the field exists is necessary because Airtable may send rows
  // with the field empty if it is not checked
  $published = (isset($row->fields->{'Approved for Publishing'}) && $row->fields->{'Approved for Publishing'});

  if($display_key != 'no' && $published) {
    $person_data = [
      "id" => $row->id,
      "given_name" => $row->fields->{'Given Name'},
      "family_name" => $row->fields->{'Family Name'},
      "institution" => @$row->fields->{'Institution'},
      "state" => @$row->fields->{'State / Region'},
      "city" => @$row->fields->{'City'},
      "gender_id" => $row->fields->{'Tags-Gender'}[0],
      "profession_id" => $row->fields->{'Tags-Profession'}[0],
      "country_id" => $row->fields->{'Tags-Country'}[0],
      "display_key" => $display_key,
      "professional_sector_ids" => @$row->fields->{'Tags-Professional Sector'},
    ];

    // If they opted to show their contact details,
    // add them
    if($display_key == 'yes') {
      $person_data['email'] = @$row->fields->{'Email'};
      $person_data['telephone'] = @$row->fields->{'Telephone'};
    }

    // Add to both group by location and the flat array
    $people['grouped'][$current_location][] = $person_data;
    $people['points'][$current_location]['member_ids'][] = $row->id;
    $people['flat'][] = $person_data;
  }

  return $people;
}, ["grouped" => [], "flat" => []]);

// Get geocoding data for each location
$geolocation_groups = []; // associative array of geolocation points, keyed by formatted address
$geolocation_list = [];   // numeric array of geolocation points
foreach($people['points'] as $location=>$group) {
  $geolocation = getGeolocation($location);

  if(!empty($geolocation)) {
    $geolocation['member_ids'] = $group['member_ids'];
    $geolocation_groups[$geolocation['formatted_address']] = $geolocation;
    $geolocation_list[] = $geolocation;
  }
}

/***********************************************
*ASSEMBLE THE MONSTER OBJECT!*******************
***********************************************/
$gams_directory = [
  // "members" => [
  //   "flat" => $people['flat'],
  //   "grouped_by_location" => $geolocation_groups
  // ],
  "members" => $people['flat'],
  "map_points" => $geolocation_list,
  "taxonomies" => [
    "professional_sectors" => $professional_sectors,
    "countries" => $countries,
    "genders" => $genders,
    "professions" => $professions,
    "displays" => $displays
  ]
];

$gams_directory_json = json_encode($gams_directory);

file_put_contents(OUTPUT_FILE, $gams_directory_json);

/***********************************************
*PUT THE CACHED DATA TO FILE********************
***********************************************/
$cache_json = json_encode($cache);
file_put_contents(CACHE_FILE, $cache_json);

/***********************************************
*FUNCTIONS AND HELPERS**************************
***********************************************/
function getTable($tableName) {
  $airtable = new Airtable(array(
    'api_key' => AIRTABLE_API_KEY,
    'base' => AIRTABLE_DATABASE
  ));

  $request = $airtable->getContent($tableName);
  $rows = [];

  do {
    $response = $request->getResponse();
    $error = $response['error'];
    $records = $response['records'];

    if($error) {
      file_put_contents(LOG_FILE, $error->message);
      die("Found an error while trying to download data from Airtable: {$error->message}\n");
    }
    else {
      $rows = array_merge($rows, $response['records']);
    }
  }

  while( $request = $response->next() );

  return $rows;
}

function getTaxonomy($tableName, $additionalFields=null) {
  $table = getTable($tableName);
  $numeric = array_map(function($row) use ($additionalFields) {
    $tag = [
      "id" => $row->id,
      "name_en" => $row->fields->English,
      "name_fr" => $row->fields->French
    ];

    if($additionalFields) {
      foreach($additionalFields as $columnName=>$fieldKey) { // FieldName is the name of the column in Airtable; 
                                                            // fieldKey is the key assigned in this array
        $tag[$fieldKey] = $row->fields->{$columnName};
      }
    }

    return $tag;
  }, $table);

  $associative = [];
  foreach($numeric as $tag) {
    $associative[$tag['id']] = $tag;
  }

  return [
    "flat" => $numeric,
    "keyed_by_id" => $associative
  ];
}

function getGeolocation($address) {
  $http = new Client();

  // Find out if the address geolocation is cached
  global $cache; // Yeah, yeah, I know that globals are yucky, but occassionally it's fine.
  $cached = @$cache['geolocation'][$address];
  if($cached) {
    return $cached;
  }

  $response = $http->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json', [
    'query' => [
      'key' => GOOGLE_GEOCODING_API_KEY,
      'address' => $address
    ]
  ]);

  $response_object = json_decode($response->getBody(), TRUE);
  $results = $response_object['results']; 

  if(count($results)) {
    $geolocation = $results[0];

    $geolocation_data = [
      'formatted_address' => $geolocation['formatted_address'],
      'lat' => $geolocation['geometry']['location']['lat'],
      'lng' => $geolocation['geometry']['location']['lng']
    ];

    $cache['geolocation'][$address] = $geolocation_data;

    return $geolocation_data;
  }
  else {
    return null;
  }
}
