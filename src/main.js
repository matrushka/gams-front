// Uncomment this line for deployment; comment this for index.html testing
require('./bootstrap');

import Vue from 'vue'
import App from './App.vue'
import i18n from './i18n'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { LMap, LTileLayer, LMarker, LTooltip } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';

// Vue.config.productionTip = false

// Configure vue axios wrapper
Vue.use(VueAxios, axios)

// Configure vue-leaflet components
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-tooltip', LTooltip);

// Reset vue-leaflet marker icons
// eslint-disable-next-line  
delete L.Icon.Default.prototype._getIconUrl  
// eslint-disable-next-line  
L.Icon.Default.mergeOptions({  
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),  
  iconUrl: require('leaflet/dist/images/marker-icon.png'),  
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')  
})

new Vue({
  el: '#gams-app',
  i18n,
  render: h => h(App),
})
